# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=abuild
pkgver=3.3.1
pkgrel=2
pkgdesc="Script to build APK packages"
url="https://code.foxkit.us/adelie/abuild"
arch="all"
license="GPL-2.0-only"
depends="fakeroot sudo pax-utils openssl apk-tools>=2.0.7-r1 libc-utils
	attr libarchive-tools pkgconf patch lzip"
if [ "$CBUILD" = "$CHOST" ]; then
	depends="$depends curl"
fi
makedepends_build="pkgconfig"
makedepends_host="openssl-dev zlib-dev"
makedepends="$makedepends_host $makedepends_build"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="abuild-rootbld:_rootbld:noarch $pkgname-doc"
options="suid !check"
pkggroups="abuild"
source="https://distfiles.adelielinux.org/source/abuild-$pkgver+adelie.tar.xz
	keyhole.patch
	"
builddir="$srcdir/$pkgname-$pkgver+adelie"

# secfixes:
#   3.3.1-r1:
#     - CVE-2019-12875

prepare() {
	default_prepare

	cd "$builddir"
	sed -i -e "/^CHOST=/s/=.*/=$CHOST/" abuild.conf
}

build() {
	cd "$builddir"
	make VERSION="$pkgver-r$pkgrel"
}

package() {
	cd "$builddir"

	make install VERSION="$pkgver-r$pkgrel" DESTDIR="$pkgdir"

	install -m 644 abuild.conf "$pkgdir"/etc/abuild.conf
	install -d -m 775 -g abuild "$pkgdir"/var/cache/distfiles
	# hardcoded to use Alpine repositories and URLs.
	# maybe rewrite some day, pending user demand.
	rm "$pkgdir"/usr/bin/apkbuild-gem-resolver
	# very broken
	rm "$pkgdir"/usr/bin/apkbuild-cpan
}

_rootbld() {
	pkgdesc="Build packages in chroot"
	depends="abuild bubblewrap gettext-tiny git"
	mkdir -p "$subpkgdir"
}

sha512sums="c358f84d198527eeeac1a1aacc101174e18f9f34f692b12a4e4deb6b32d5f3ebdc703c90de6a6f81f195a81eebb90a11f9bfea13ef5db531d9b5d5ae6ecd6a66  abuild-3.3.1+adelie.tar.xz
757d750d4b5c88bf00774b64f2b93a9461e03f284d9423dc58c581e1309f276628de3114fcb510afd7c3cd55ceb721c1278e42756977c97ebe2597207805318d  keyhole.patch"
