# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grep
pkgver=3.3
pkgrel=0
pkgdesc="Searches input files for lines containing a match to a specified pattern"
url="https://www.gnu.org/software/grep/grep.html"
arch="all"
license="GPL-3.0+"
makedepends="pcre-dev autoconf automake"
checkdepends="coreutils bash diffutils"
subpackages="$pkgname-doc $pkgname-lang"
source="https://mirrors.kernel.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	gnulib-tests-dont-require-gpg-passphrase.patch
	fix-tests.patch"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--bindir=/bin \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="e4805dbddf7cd0f0faf412557d408509650c1ccf703bc450f10a3f727c690dbfaa1235aa81939a0e4b7ac6190f88c15ea1fcc562b343d4b4c7550f967aeb15db  grep-3.3.tar.xz
fd97fccd661befc558b3afb3e32c82dd2cef511a05e6129d49540599297c1b59ab1f109e63a12f585a2348c26c28fb98330c348829d1fe61cf8149d0dd1c989c  gnulib-tests-dont-require-gpg-passphrase.patch
9ba6b01c0c74933299afb469dadd2ea0c7e24befa34c691671a576063e32a1f0c735541e5e2bb0073d8afd814790909f7f895827aa8a2fbacdfcae380a7bcb11  fix-tests.patch"
