# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=nvi
pkgver=1.79
pkgrel=1
pkgdesc="Berkeley text editor"
url="https://sites.google.com/a/bostic.com/keithbostic/vi/"
arch="all"
options="!check"  # No test suite.
license="BSD-4-Clause-UC"
depends=""
makedepends="ncurses-dev"
subpackages="$pkgname-doc"
source="https://sites.google.com/a/bostic.com/keithbostic/files/nvi-$pkgver.tar.gz
	build-fix.patch
	"

build() {
	cd "$builddir"/build
	# Note!  --disable-curses means disable *builtin* curses.
	# It makes vi(1) use ncurses instead.  That is what we want.
	LIBS="-ltinfow" ./configure \
		--prefix=/usr \
		--mandir="$pkgdir"/usr/share/man \
		--build=$CBUILD \
		--host=$CHOST \
		--disable-curses
	make
}

package() {
	cd "$builddir"/build
	mkdir -p "$pkgdir"
	make prefix="$pkgdir"/usr install
	mv "$pkgdir"/usr/bin/ex "$pkgdir"/usr/bin/ex.nvi
	mv "$pkgdir"/usr/share/man/cat1/ex.0 "$pkgdir"/usr/share/man/cat1/ex.nvi.0
	mv "$pkgdir"/usr/share/man/man1/ex.1 "$pkgdir"/usr/share/man/man1/ex.nvi.1
	mv "$pkgdir"/usr/bin/view "$pkgdir"/usr/bin/view.nvi
	mv "$pkgdir"/usr/share/man/cat1/view.0 "$pkgdir"/usr/share/man/cat1/view.nvi.0
	mv "$pkgdir"/usr/share/man/man1/view.1 "$pkgdir"/usr/share/man/man1/view.nvi.1
}

sha512sums="d1d8adb2aeeee127f5e6fe5cc8b9e3e26fe22084075d17cceecd9cee7956131becee764d721af69626d60d6d75873d5073dc9817995f0274ed307e044921a1d0  nvi-1.79.tar.gz
591f28094731a438841ce806db870592b659dbe2548de438005adb706d2dab0d9817531b07c77c6aa3c8d6cfc7739b485c6835aed174cb84b219f978a5ae5f51  build-fix.patch"
