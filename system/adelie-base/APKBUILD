# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=adelie-base
pkgver=0.9.3
pkgrel=0
pkgdesc="The Adélie Linux Base System"
url="https://www.adelielinux.org/"
arch="noarch"
options="!check !fhs"
license="NCSA"
_core_depends="apk-tools adelie-keys musl-utils shadow
	shimmy grep procps /bin/sh
	coreutils diffutils findutils
	util-linux net-tools
	nvi sed psmisc less tzdata"
depends="adelie-core binutils console-setup debianutils file gettys-openrc
	libarchive-tools man-db patch sed sharutils sysklogd zsh"
makedepends=""
makedepends_host="$depends"
subpackages="adelie-core:core $pkgname-doc $pkgname-posix $pkgname-lsb dev-kit:devkit docs"
provides="alpine-base"
source="https://distfiles.adelielinux.org/source/$pkgname/$pkgname-$pkgver.tar.xz
	group
	passwd
	addgroup
	adduser
	"

build() {
	# taken from Alpine Base Layout: generate shadow
	awk -F: '{
		pw = ":!:"
		if ($1 == "root") { pw = "::" }
		print($1 pw ":0:::::")
	}' "$srcdir"/passwd > "$builddir"/tree/etc/shadow
	chmod 640 "$builddir"/tree/etc/shadow
}

package() {
	mkdir -p "$pkgdir"
	make install DESTDIR="$pkgdir"
	install -m644 "$srcdir"/group "$pkgdir"/etc/group
	install -m644 "$srcdir"/passwd "$pkgdir"/etc/passwd
	install -m640 -g shadow "$builddir"/tree/etc/shadow "$pkgdir"/etc/shadow
	chmod 1777 "$pkgdir"/var/tmp
	echo '' > "$pkgdir"/etc/shells

	# stupid inflatable stupid busybox
	local i
	for i in adduser addgroup; do
		install -m755 "$srcdir"/$i "$pkgdir"/usr/sbin/$i
	done
}

core() {
	core="The Adélie Linux minimal runtime environment"
	depends=$_core_depends
	replaces="adelie-base"
	mkdir -p "$subpkgdir"
	mv -i "$pkgdir"/* "$subpkgdir"/
}

doc() {
	local _doc DOCS
	DOCS="CONTRIBUTING.rst LICENSE README.rst"
	for _doc in $DOCS; do
		install -Dm644 "$srcdir"/$pkgname-$pkgver/$_doc \
			"$subpkgdir"/usr/share/doc/$pkgname/$_doc
	done
}

posix() {
	# We pull in vim for /usr/bin/ex, until apk has an alternatives system
	depends="adelie-base at bc cflow cxref ed fcron heirloom-devtools
		heirloom-pax mailx mawk uucp utmps vim"
	pkgdesc="$pkgdesc - Additional POSIX tools"
	mkdir -p "$subpkgdir"
	return 0
}

lsb() {
	depends="adelie-base gettext-tiny gzip libarchive-tools linux-pam make
		mawk ncurses nspr nss"
	pkgdesc="$pkgdesc - Additional LSB tools"
	mkdir -p "$subpkgdir"
	return 0
}

devkit() {
	depends="build-tools libarchive-tools"
	pkgdesc="The Adélie Linux Development Kit"
	mkdir -p "$subpkgdir"
	return 0
}

docs() {
	depends=""
	pkgdesc="Metapackage that will magically install all documentation for your system"
	mkdir -p "$subpkgdir"
	return 0
}

sha512sums="a9690b20f90b94bda458e0570db55ce835e3d34ef04a3ea3f3d77e2d97483cecb940dbc3a1f2def72ff2589cc5b9cbe42ae38dc25d9b9ca25960a4832c87c7b4  adelie-base-0.9.3.tar.xz
37260d9315fd8d8b0df3dfb5bd3e1035a47ba1e43f33aa195e3feca6d169da282c1b067ef4603e7e9acaedbfef8b58cf490c00bdca444d43c9d570355ad29eac  group
3ee7ebe2ca762029fa1795471735922416a2ec7683774d107b38bcd7aeb8277046edeff0c26b1f1195739478e1e11fb7a51c55149cf79adab44ed217334b7013  passwd
f2437ebfbc83aa6eaeec3a111aae98751215cebfaec3991ccc511be8c8938778ae46f15e07c7306bd431d036bc4ba49b33b724b839c125bd882d2e93c0314ae8  addgroup
2f97bb4b24d9ea8d8d3001bb0cd0aac4b65caff75d25b172f9e925c55185183c2d4b54d9949cd8d43ca74c003a8590f2b98a119696075507f513b549fac94476  adduser"
