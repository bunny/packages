# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libssh2
pkgver=1.9.0
pkgrel=1
pkgdesc="Library for accessing SSH servers"
url="https://libssh2.org/"
arch="all"
license="BSD-3-Clause"
checkdepends="openssh-server"
makedepends="openssl-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.libssh2.org/download/libssh2-$pkgver.tar.gz
	test-sshd.patch
	CVE-2019-17498.patch"

# secfixes:
#   1.9.0-r1:
#     - CVE-2019-17498

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-rpath
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="41a3ebcf84e32eab69b7411ffb0a3b6e6db71491c968602b17392cfe3490ef00239726ec28acb3d25bf0ed62700db7f4d0bb5a9175618f413865f40badca6e17  libssh2-1.9.0.tar.gz
eef3c43184d53a3c655915ad61d182a88d9cced75ba8f8dde73ccf771ff4aeaa0f26e95aeb53601d7c47d96a2421c98678e9baf497f3883faa4427a091eea62c  test-sshd.patch
102542a2023d53f7684c99a89fa4c592ee4ababc09bc174c52cd20f7f21b4c5878a89bae7e20c3438490666b7b0758caba5cf82c2b2965e1e58e02d5c1f4ea47  CVE-2019-17498.patch"
