# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cvs
pkgver=1.11.23
pkgrel=2
pkgdesc="Concurrent Versions System"
url="https://www.nongnu.org/cvs/"
arch="all"
options="!check"  # "Note that the test can take an hour or more to run"
license="GPL-2.0+"
depends=""
makedepends="zlib-dev"
install=
subpackages="$pkgname-doc"
source="https://ftp.gnu.org/non-gnu/cvs/source/stable/$pkgver/$pkgname-$pkgver.tar.gz
	cvs-musl.patch
	CVE-2010-3846.patch
	CVE-2017-12836.patch
	"

# secfixes:
#   1.11.23-r2:
#     - CVE-2010-3846
#     - CVE-2017-12836

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-external-zlib \
		--with-tmpdir=/tmp
	make
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="e486df1d2aaf13605b9abc8ea5e8e2261dd015483cef82a9489919646f0d5d52a7bf4385f4fdb5f845a9c2287184153a0d456510089f1e2609957ba48ad9f96a  cvs-1.11.23.tar.gz
7de04d5ec797430f8405b00e271d9edb5dffa3be855fc1e1dc35b134d981418c969486da668a78e1da88a4dba57952bfa14ffafbe3ff3ffc081de9cc908cf245  cvs-musl.patch
eed761af81c9bcd3edd898559e9be25c6612bdef19984cc6380a08039525179fa34d9ade6c55c1b4f23e495156b34cafeab3e63cfd120c0e68a42aa7992e5e85  CVE-2010-3846.patch
2775f5bde63d7eaee8c8f7467a8b43d533abbc172cf6b2d6ca7088203133a135e4e6a2a8028191d0102300913165dbd54fcf1f43683e742cb32f04ab06aca121  CVE-2017-12836.patch"
