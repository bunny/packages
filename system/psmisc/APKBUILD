# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer:
pkgname=psmisc
pkgver=23.2
pkgrel=0
pkgdesc="Miscellaneous utilities that use the proc filesystem"
url="https://gitlab.com/psmisc/psmisc"
arch="all"
options="!check"  # killall(8) is known-broken on musl:
                  # https://gitlab.com/psmisc/psmisc/issues/18
license="GPL-2.0+"
makedepends_build="autoconf>=2.69 automake"
makedepends_host="ncurses-dev"
checkdepends="dejagnu"
subpackages="$pkgname-doc"
[ "$CBUILD" != "$CHOST" ] || subpackages="$subpackages $pkgname-lang"
source="$pkgname-$pkgver.tar.bz2::https://gitlab.com/$pkgname/$pkgname/repository/archive.tar.bz2?ref=v$pkgver
	fix-peekfd-on-ppc.patch
	musl_ptregs.patch
	"

prepare() {
	ln -fs $pkgname-v$pkgver-* "$builddir"

	default_prepare

	cd "$builddir"
	sh autogen.sh
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-harden-flags \
		--enable-ipv6 \
		--disable-selinux \
		ac_cv_func_malloc_0_nonnull=yes \
		ac_cv_func_realloc_0_nonnull=yes
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="210dd327f1ff614587ff5b4ae60cd843426302a020fe7f1c018ddba99dcdff7b7ad0620e2b1f10f6204402c1dc0deebcb4332235bcca082d6be21dcb68043619  psmisc-23.2.tar.bz2
a910611896368a088503f50a04a1c2af00d57ee20f3613e81c79cd89574805a505dff43e356ed833a464e3b59d7c1e11fd52cf0bbf32fcfece4dbd2380f23b71  fix-peekfd-on-ppc.patch
57e3ebf427bae9d3ef5b40be1cbc29743009720a75369c91d338f7634fea5bf522f20ec54404b99f3a39e2c4c146c40baee2b22f68e5ede76678d488fde00db5  musl_ptregs.patch"
