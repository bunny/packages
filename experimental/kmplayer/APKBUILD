# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmplayer
pkgver=0.12.0b
pkgrel=0
pkgdesc="KDE video player"
url="https://kmplayer.kde.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev xcb-util-dev kconfig-dev
	xcb-util-cursor-dev xcb-util-keysyms-dev xcb-util-wm-dev qt5-qtsvg-dev
	qt5-qtx11extras-dev kcoreaddons-dev kinit-dev ki18n-dev kparts-dev
	kdelibs4support-dev kio-dev kwidgetsaddons-dev kmediaplayer-dev
	cairo-dev gtk+2.0-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang $pkgname-libs $pkgname-plugin"
source="https://download.kde.org/stable/kmplayer/0.12/kmplayer-$pkgver.tar.bz2"
builddir="$srcdir/kmplayer-$pkgver"

build() {
	cd "$builddir"
	mkdir build
	cd build
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		..
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="d34c496d0c24c6d5c1bb675b83fac60da5c9672e4076aed72bac2e9b333e88d031076e7e386ec7b61ae8ece88870f6187c61f483f425aa78edd1e443c7f2e882  kmplayer-0.12.0b.tar.bz2"
