# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=otter-browser
pkgver=0.9.96
pkgrel=0
pkgdesc="A free, open browser focused on user privacy"
url="https://otter-browser.org/"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="cmake qt5-qtbase-dev qt5-qttools-dev openssl-dev gstreamer-dev
	qt5-qtmultimedia-dev qt5-qtdeclarative-dev qt5-qtsvg-dev hunspell-dev
	qt5-qtxmlpatterns-dev qt5-qtwebkit-dev"
install=""
langdir="/usr/share/otter-browser/locale"
subpackages="$pkgname-doc $pkgname-lang"
source="https://sourceforge.net/projects/$pkgname/files/$pkgname-rc6/$pkgname-$pkgver.tar.bz2"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4ba915226360ae1d6f5fd9bf5ea502ea9f2104594536eff0cfe57f162fb769158b03da2da9e5c0ce757af8fafa6f379ce81391b648bf4253c2bb35f12c3620b3  otter-browser-0.9.96.tar.bz2"
