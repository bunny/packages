# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=boost
pkgver=1.69.0
_pkgver=$(printf '%s' "$pkgver" | tr . _)
pkgrel=0
pkgdesc="Free peer-reviewed portable C++ source libraries"
url="https://www.boost.org/"
arch="all"
license="BSL-1.0"
options="!check"  # No test suite.
depends=""
depends_dev="linux-headers"
makedepends="$depends_dev byacc bzip2-dev flex python3-dev xz-dev zlib-dev
	zstd-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="http://downloads.sourceforge.net/$pkgname/${pkgname}_$_pkgver.tar.bz2
	boost-1.57.0-python-abi_letters.patch
	boost-1.57.0-python-libpython_dep.patch
	"
builddir="$srcdir/${pkgname}_${_pkgver}"

_libs="
	atomic
	chrono
	container
	contract
	coroutine
	date_time
	fiber
	filesystem
	graph
	iostreams
	math
	prg_exec_monitor
	program_options
	python3
	random
	regex
	serialization
	system
	thread
	unit_test_framework
	wave
	wserialization
	"
for _lib in $_libs; do
	subpackages="$subpackages $pkgname-$_lib:_boostlib"
done

prepare() {
	default_prepare

	PY3_VERSION="$(_pyversion python3)"
	abiflags=$(python3-config --abiflags)


	# create user-config.jam
	cat > user-config.jam <<-__EOF__

	using gcc : : $CC : <cxxflags>"${CXXFLAGS}" <linkflags>"${LDFLAGS}" ;
	using python : ${PY3_VERSION} : /usr/bin/python3 : /usr/include/python${PY3_VERSION}m : : : : $abiflags ;

	__EOF__
}

case "$CARCH" in
	armhf|armv7|aarch64) _boostarch=arm ;;
	ppc64*) _boostarch=ppc ;;
	s390x) _boostarch=s390 ;;
	pmmx) _boostarch=x86 ;;
	*) _boostarch=$CARCH ;;
esac
_enginedir=tools/build/src/engine
_bjam="${builddir}/$_enginedir/bin.linux${_boostarch}/bjam"
[ "$_boostarch" = "s390" ] && _bjam="${builddir}/$_enginedir/bin.linux390/bjam" && \
	_options_s390="--without-coroutine --without-coroutine2"

# context is broken on at least s390 and sparc
_options="--user-config=\"$builddir/user-config.jam\"
	--prefix=\"$pkgdir/usr\"
	release
	debug-symbols=off
	runtime-link=shared
	link=shared,static
	cflags=-fno-strict-aliasing
	-sPYTHON_ROOT=/usr
	-sTOOLS=gcc
	--layout=system
	-q
	--without-context
	-j${JOBS:-2}
	${_options_s390}
	"

build() {
	export BOOST_ROOT="$builddir"

	msg "Building bjam"
	cd "$builddir"/$_enginedir
	CC= ./build.sh cc

	msg "Building bcp"
	cd "$builddir"/tools/bcp
	"$_bjam" -j${JOBS:-2}

	msg "Building boost"
	cd "$builddir"
	"$_bjam" $_options
}

package() {
	export BOOST_ROOT="$builddir"

	install -Dm755 $_bjam \
		"$pkgdir"/usr/bin/bjam

	install -Dm755 dist/bin/bcp "$pkgdir"/usr/bin/bcp

	install -Dm644 LICENSE_1_0.txt \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE_1_0.txt

	"$pkgdir"/usr/bin/bjam $_options \
		--includedir="$pkgdir"/usr/include \
		--libdir="$pkgdir"/usr/lib \
		install
}

_boostlib() {
	local name="${subpkgname#$pkgname-}"
	pkgdesc="Boost $name library"
	case "$name" in
	python*) depends="$depends $name"
	esac

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libboost_$name* "$subpkgdir"/usr/lib/
}

_pyversion() {
	$1 -c 'import sys; print("%i.%i" % (sys.version_info.major, sys.version_info.minor))'
}

sha512sums="d0e9bb858c44880d56c0291afef6a1b011a62f659a2d8f58dcb6147ea0899f9157bd8db3097896618fee0116847ebeac78b6d0f0fec8a92c3469500828bbe552  boost_1_69_0.tar.bz2
d96d4d37394a31764ed817d0bc4a99cffa68a75ff1ecfd4417b9e1e5ae2c31a96ed24f948c6f2758ffdac01328d2402c4cf0d33a37107e4f5f721e636daebd66  boost-1.57.0-python-abi_letters.patch
132c4b62815d605c2d3c9038427fa4f422612a33711d47b2862f2311516af8a371d6b75bf078a7bffe20be863f8d21fb9fe74dc1a1bac3a10d061e9768ec3e02  boost-1.57.0-python-libpython_dep.patch"
