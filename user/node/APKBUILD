# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=node
pkgver=10.16.3
pkgrel=0
pkgdesc="JavaScript runtime"
url="https://nodejs.org/"
arch="all"
license="MIT AND ICU AND BSD-3-Clause AND BSD-2-Clause AND ISC AND OpenSSL AND Public-Domain AND Zlib AND Artistic-2.0 AND Apache-2.0 AND CC0-1.0"
depends=""
makedepends="c-ares-dev http-parser-dev icu-dev libexecinfo-dev libuv-dev
	nghttp2-dev python3 zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://nodejs.org/download/release/v$pkgver/node-v$pkgver.tar.xz
	https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tar.xz
	libatomic.patch
	ppc32.patch
	ppc64.patch
	stack-silliness.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

# secfixes:
#   10.16.3-r0:
#     - CVE-2019-9511
#     - CVE-2019-9512
#     - CVE-2019-9513
#     - CVE-2019-9514
#     - CVE-2019-9515
#     - CVE-2019-9516
#     - CVE-2019-9517
#     - CVE-2019-9518

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-2.7.15"
	[ -d ../python ] && rm -r ../python
	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence
	./configure --prefix="$srcdir/python"
	make -j $JOBS
	make -j $JOBS install
}

build() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	# We can't use --shared-openssl until 1.1 is available.
	python ./configure.py \
		--prefix=/usr \
		--with-intl=system-icu \
		--shared-cares \
		--shared-http-parser \
		--shared-libuv \
		--shared-nghttp2 \
		--openssl-no-asm \
		--shared-zlib
	# keep DESTDIR set, to avoid a full rebuild in package()
	make DESTDIR="$pkgdir"
}

check() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	make DESTDIR="$pkgdir" test-only
}

package() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	make DESTDIR="$pkgdir" install
}

sha512sums="5d5457adaae87e41699cdf5bd62d32e9590248f7dadd583614cca9078a1749a4ee477433ebbe4278ec872fdfc377d99097c1e06073e0fd47c8c6597256ccdeb7  node-v10.16.3.tar.xz
27ea43eb45fc68f3d2469d5f07636e10801dee11635a430ec8ec922ed790bb426b072da94df885e4dfa1ea8b7a24f2f56dd92f9b0f51e162330f161216bd6de6  Python-2.7.15.tar.xz
8f64922d586bce9d82c83042a989739cc55ecc5e015778cdfbda21c257aa50527ddb18740985bcb2068e4a749b71eb8a135d9a8152b374d361589df7f33c9b60  libatomic.patch
6d37794c7c78ef92ebb845852af780e22dc8c14653b63a8609c21ab6860877b9dffc5cf856a8516b7978ec704f312c0627075c6440ace55d039f95bdc4c85add  ppc32.patch
583326353de5b0ac14a6c42321f6b031bd943a80550624794e15bd7526470f67bfa14a66558db3c94b4ee2db3053d2e4efed2117f4e7b6dca3c59c171048c094  ppc64.patch
3ea09e36ed0cc31e0475ebc9c92b7609b70e9c1637c5db6c92cf1d6363fb8c6f884ffa20dd81054ca390b721695185327d80c9eeff0688a959e9d46947602471  stack-silliness.patch"
