# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gst-plugins-base
pkgver=1.16.1
pkgrel=0
pkgdesc="GStreamer multimedia framework - Base plugins"
url="https://gstreamer.freedesktop.org/"
arch="all"
options="!check"  # fails overlaycomposition on ppc64
license="GPL LGPL"
depends=""
checkdepends="orc-compiler"
makedepends="alsa-lib-dev cdparanoia-dev expat-dev glib-dev
	gobject-introspection-dev gstreamer-dev libice-dev libogg-dev libsm-dev
	libtheora-dev libvorbis-dev libx11-dev libxt-dev libxv-dev mesa-dev
	opus-dev orc-dev pango-dev perl cmd:which !gst-plugins-base"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
replaces="gst-plugins-base1"
source="https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-$pkgver.tar.xz"
ldpath="/usr/lib/gstreamer-1.0"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-static \
		--disable-experimental \
		--disable-fatal-warnings \
		--with-default-audiosink=alsasink \
		--enable-introspection \
		--with-package-name="GStreamer Base Plugins (${DISTRO_NAME:-Adélie Linux})" \
		--with-package-origin="${DISTRO_URL:-https://www.adelielinux.org/}"
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

doc() {
	default_doc
	replaces="${pkgname}1-doc"
}

sha512sums="ce65b5d63d3ac6c20b051d477d09515bfb1c2e4b10c58346180265bdc4ae44dd9833727b5eea5fe9c409a3cd8874429be23654c7a5c23e881c0e8f9e89fb32c9  gst-plugins-base-1.16.1.tar.xz"
