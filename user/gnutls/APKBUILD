# Contriburo: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: 
pkgname=gnutls
pkgver=3.6.9
pkgrel=1
pkgdesc="A TLS protocol implementation"
url="http://www.gnutls.org/"
arch="all"
options="!check"  # https://gitlab.com/gnutls/gnutls/issues/560
license="LGPL-2.1+"
makedepends="libtasn1-dev libunistring-dev nettle-dev p11-kit-dev texinfo
	unbound-dev cmd:which zlib-dev"
checkdepends="diffutils"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-dev $pkgname-lang
	$pkgname-c++:xx $pkgname-dane $pkgname-utils"
_v=${pkgver%.*}
_v2=$pkgver
case $pkgver in
*.*.*.*)
	_v=${_v%.*}
	_v2=${pkgver%.*}
	;;
esac
source="https://www.gnupg.org/ftp/gcrypt/gnutls/v${_v}/$pkgname-$pkgver.tar.xz
	gnulib-tests-dont-require-gpg-passphrase.patch"
builddir="$srcdir/$pkgname-$_v2"

# secfixes:
#   3.5.13-r0:
#     - CVE-2017-7507

build() {
	LIBS="-lgmp" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-openssl-compatibility \
		--disable-rpath \
		--disable-static \
		--disable-guile \
		--disable-valgrind-tests \
		--without-included-libtasn1 \
		--enable-cxx \
		--enable-manpages \
		--enable-tests \
		--disable-full-test-suite \
		--with-p11-kit
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

dane() {
	pkgdesc="$pkgdesc (DNS DANE support library)"
	mkdir -p "$subpkgdir"/usr/bin
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/bin/danetool "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgnutls-dane* "$subpkgdir"/usr/lib/
}

utils() {
	pkgdesc="Command line tools for TLS protocol"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

xx() {
	pkgdesc="The C++ interface to GnuTLS"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*xx.so.* "$subpkgdir"/usr/lib/
}

sha512sums="a9fd0f4edae4c081d5c539ba2e5574a4d7294bc00c5c73ea25ce26cb7fd126299c2842a282d45ef5cf0544108f27066e587df28776bc7915143d190d7d5b9d07  gnutls-3.6.9.tar.xz
abc24ee59cc67805fe953535b0bae33080fc8b0bf788304377f6d10ec8c162c4cf203a69c98a4ba3483b4c60ed7a204433cc7db9b8190eddb0d68f6fb6dad52d  gnulib-tests-dont-require-gpg-passphrase.patch"
