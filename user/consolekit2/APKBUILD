# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgbase=ConsoleKit2
pkgname=consolekit2
pkgver=1.2.1
pkgrel=1
pkgdesc="Framework for defining and tracking users, login sessions, and seats"
provides="consolekit=$pkgver"
replaces="consolekit"
arch="all"
url="https://consolekit2.github.io/ConsoleKit2"
license="GPL-2.0+"
depends="eudev polkit"
makedepends="acl-dev docbook-xml eudev-dev glib-dev libdrm-dev libevdev-dev
	libnih-dev libxslt-dev linux-pam-dev polkit-dev xmlto xorg-server-dev
	zlib-dev"
checkdepends="libxml2-utils"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-openrc"
source="https://github.com/ConsoleKit2/$pkgbase/releases/download/$pkgver/$pkgbase-$pkgver.tar.bz2
	consolekit2.initd
	pam-foreground-compat.ck
	poweroff.patch"
# Capital "ConsoleKit"
builddir="$srcdir"/$pkgbase-$pkgver

build() {
	cd "$builddir"
	XMLTO_FLAGS='--skip-validation' ./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-static \
		--enable-pam-module \
		--enable-udev-acl \
		--enable-tests \
		--enable-docbook-docs \
		--enable-polkit
	sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	install -m 755 "$srcdir"/pam-foreground-compat.ck \
		"$pkgdir"/usr/lib/ConsoleKit/run-session.d/
	install -D -m755 "$srcdir"/consolekit2.initd \
		"$pkgdir"/etc/init.d/consolekit
}

sha512sums="31befe89f7fa604138bfb0722fc6cf12f0934bac004f98fc331004eb5a7f466ed7bd0dc9adc9869da739974208f9a3bc125068ff8a60d4b2badb58ef70a3eb10  ConsoleKit2-1.2.1.tar.bz2
8c16c452707475bdd4a50d3ade367d52ad92a6560be48b4e21e5b5eadef6e56c39d3d03d3a64f9b45a59eca50179cf5aa9c11978904d5d101db7498fb9bc0339  consolekit2.initd
3b114fbbe74cfba0bfd4dad0eb1b85d08b4979a998980c1cbcd7f44b8a16b0ceca224680d4f4a1644cd24698f8817e5e8bdfcdc4ead87a122d0e323142f47910  pam-foreground-compat.ck
033595766671f545ba6c9f3fcb529547d20359cdd8eb901bb7a6c3b319b495038e8072e3b01f2fd264f592b0c7825a79282d8bc590f057a5f62e9fdfedde9c68  poweroff.patch"
