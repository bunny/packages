# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kget
pkgver=19.08.1
pkgrel=0
pkgdesc="Versatile download manager"
url="https://www.kde.org/applications/internet/kget/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcompletion-dev ki18n-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kio-dev
	kdoctools-dev kiconthemes-dev kitemviews-dev kcmutils-dev kparts-dev
	kdelibs4support-dev knotifications-dev knotifyconfig-dev kservice-dev
	solid-dev ktextwidgets-dev kwallet-dev kwidgetsaddons-dev kxmlgui-dev
	kwindowsystem-dev gpgme-dev qca-dev boost-dev libktorrent-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kget-$pkgver.tar.xz
	kf5.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="bd13ada5269b9658ffc921b0718d6690f3de73ed7ebd2a112cbfadda71491f8f2557e4e8c82b5af2e1ecd21f5b7085e4dfff705141c605fee03348ab50261521  kget-19.08.1.tar.xz
d66fb3988aa26c2cb9e243ef446679e715cb3bd10ee8cb1b0b39a68d424ab9fca71428fa0c6004a1671093201e71e371331b069d6bb0325d0e7b936a9b95a73c  kf5.patch"
