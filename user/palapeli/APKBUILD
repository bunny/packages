# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=palapeli
pkgver=19.08.1
pkgrel=0
pkgdesc="Jigsaw puzzle game by KDE"
url="https://www.kde.org/applications/games/palapeli/"
arch="all"
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	karchive-dev kcompletion-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kcrash-dev ki18n-dev kio-dev kitemviews-dev
	knotifications-dev kservice-dev kwidgetsaddons-dev kxmlgui-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/palapeli-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="319e14d887678a243f55dd9f06cea6437a91b46e85f508eeeca22b8eeaa529af1df7b8e7ca594b2a23d530face26f9c4ad9869440d849827ae59dd95d5940078  palapeli-19.08.1.tar.xz"
