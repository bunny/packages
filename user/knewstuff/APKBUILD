# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knewstuff
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for discovering and downloading plugins, themes, and more"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev attica-dev kconfig-dev kservice-dev kxmlgui-dev
	openssl-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev karchive-dev kcompletion-dev kcoreaddons-dev kio-dev
	ki18n-dev kiconthemes-dev kirigami2-dev kitemviews-dev
	ktextwidgets-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knewstuff-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# kmoretoolstest requires X11.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'kmoretoolstest'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="96fd8100c78e7d4f9c0ca26ad12587f952455b302ae195184e6fb0ae601b2806668cf2b6dca012e08441622fd72e99c30a52fe901d7af60c4b59701d72b1196a  knewstuff-5.54.0.tar.xz"
