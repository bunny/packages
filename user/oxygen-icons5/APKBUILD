# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=oxygen-icons5
pkgver=5.54.0
pkgrel=0
pkgdesc="The KDE Oxygen icon set"
url="https://www.kde.org/"
arch="noarch"
options="!check"  # fdupes, same thing as breeze-icons
license="LGPL-2.1"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev"
subpackages=""
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/oxygen-icons5-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="5ae34015aebff31cab3192905c388ac5a75c67e7b24ba3bb12075edf6f6cb013f9ace837ea8dd4ef7c5174e27ea1b84e418b34eb983648943c4063dd076d6ccb  oxygen-icons5-5.54.0.tar.xz"
