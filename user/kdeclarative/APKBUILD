# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdeclarative
pkgver=5.54.0
pkgrel=0
pkgdesc="Frameworks for creating KDE components using QML"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires accelerated X11 desktop running
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kiconthemes-dev kio-dev
	kpackage-dev libepoxy-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="57a042a3c9be486b9582f133a0d6688758d1ae2dd4079168d3830cbd6b2d656b22d7b1fa321f77c1d14e216e2714984303db943df623b71d29c87d7c410871c2  kdeclarative-5.54.0.tar.xz"
