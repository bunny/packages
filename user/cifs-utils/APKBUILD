# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=cifs-utils
pkgver=6.9
pkgrel=0
pkgdesc="CIFS filesystem user-space tools"
url="https://wiki.samba.org/index.php/LinuxCIFS_utils"
arch="all"
options="!check suid"  # No test suite.
license="GPL-3.0+ AND GPL-2.0+ AND LGPL-3.0+"
depends=""
makedepends="keyutils-dev krb5-dev libcap-ng-dev linux-pam-dev
	py3-docutils talloc-dev"
subpackages="$pkgname-doc $pkgname-dev"
source="https://ftp.samba.org/pub/linux-cifs/$pkgname/$pkgname-$pkgver.tar.bz2
	musl-fix-includes.patch
	xattr_size_max.patch"

build() {
	# --enable-cifsidmap and --enable-cifsacl require libwbclient (samba)

	autoreconf -i
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-cifsidmap=no \
		--enable-cifsacl=no
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	# Allow unprivileged mount
	chmod u+s "$pkgdir"/sbin/mount.cifs
}

sha512sums="b92e4e39eeed1032bb175659296cde034703fb3ca63aae00419d46a33dadf821fedaf03734128112c164c84bcbb48d92d03cdc275c4a7cba26f984aeca40a40a  cifs-utils-6.9.tar.bz2
99a2fab05bc2f14a600f89526ae0ed2c183cfa179fe386cb327075f710aee3aed5ae823f7c2f51913d1217c2371990d6d4609fdb8d80288bd3a6139df3c8aebe  musl-fix-includes.patch
2a9366ec1ddb0389c535d2fa889f63287cb8374535a47232de102c7e50b6874f67a3d5ef3318df23733300fd8459c7ec4b11f3211508aca7800b756119308e98  xattr_size_max.patch"
