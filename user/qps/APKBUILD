# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=qps
pkgver=1.10.20
pkgrel=0
pkgdesc="Qt-based task manager"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qttools-dev libxrender-dev lxqt-build-tools>=0.6.0"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/qps/releases/download/$pkgver/qps-$pkgver.tar.xz
	qps-1.10.17-compile-fixes.patch"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="407130ac12f1e68da3e22543634d101c7a8353363ff1d9c6edbf98ce5d48f149aa5d937985f00109b40bd3cdb0b9a8e9745e845dd921be4aaf119022632bb9ef  qps-1.10.20.tar.xz
32c154f1d3c9e74dda3eabfd2e3b9e9f3c1d77f808dfacc9dd05c904066832d92d361ca56ef9d784d945fad60cf67d6c909cbb730a0ffed1fea3bf44c3aad5a2  qps-1.10.17-compile-fixes.patch"
