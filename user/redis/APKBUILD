# Contributor: V.Krishn <vkrishn4@gmail.com>
# Maintainer: Alyx Wolcott <alyx@leuhta.com>
pkgname=redis
pkgver=5.0.6
pkgrel=0
pkgdesc="Advanced key-value store"
url="https://redis.io/"
arch="all"
license="BSD-3-Clause"
depends=""
makedepends="linux-headers"
checkdepends="tcl"
subpackages="$pkgname-openrc"
install="redis.pre-install"
pkgusers="redis"
pkggroups="redis"
source="http://download.redis.io/releases/$pkgname-$pkgver.tar.gz
	fix-ppc-atomics.patch
	posix-runtest.patch
	redis.initd
	redis.logrotate
	redis.confd
	"

# secfixes:
#   4.0.14-r0:
#     - CVE-2019-10192
#     - CVE-2019-10193

prepare() {
	default_prepare

	sed -i -e 's|^daemonize .*|daemonize yes|' \
		-e 's|^dir .*|dir /var/lib/redis/|' \
		-e 's|^logfile .*|logfile /var/log/redis/redis\.log|' \
		-e 's|^pidfile .*|pidfile /var/run/redis/redis\.pid|' \
		-e 's|^loglevel .*|loglevel notice|' \
		redis.conf

	# disable broken tests
	# see: https://github.com/antirez/redis/issues/2814
	#      https://github.com/antirez/redis/issues/3810

	sed -i -e '/integration\/aof/d' \
	       -e '/integration\/logging/d' \
		tests/test_helper.tcl
}

build() {
	make PREFIX=/usr \
		INSTALL_BIN="$pkgdir"/usr/bin \
		MALLOC=libc \
		FINAL_LIBS="-latomic " \
		all
}

check() {
	make test
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	install -d -o redis -g redis \
		"$pkgdir"/var/lib/redis \
		"$pkgdir"/var/log/redis \
		"$pkgdir"/var/run/redis

	install -D -m755 "$builddir/COPYING" \
		"$pkgdir/usr/share/licenses/redis/COPYING"
        install -D -m755 "$srcdir/redis.initd" "$pkgdir/etc/init.d/redis" \
		&& install -Dm644 "$srcdir/redis.logrotate" \
			"$pkgdir/etc/logrotate.d/redis" \
		&& install -Dm644 "$srcdir/redis.confd" \
			"$pkgdir/etc/conf.d/redis"
        install -D -m644 "$builddir/redis.conf" "$pkgdir/etc/redis.conf"

	make PREFIX=/usr \
		INSTALL_BIN="$pkgdir/usr/bin" \
		install
}

sha512sums="19a4738ec4c3ac077d57857559557685c56b0ac58ccfff5aac88a099894fa9a2bbe18ef126522960e38d6c0e8b0f9a00755879305c07caaa5add41bc677d44f3  redis-5.0.6.tar.gz
f768acea3e1868dbf0596085640c83e58d899860d7d647b0965fa858844c494d0a49b229fb417456d83f3e2690e5450950c31e0fa40529df85a9cde38d8981c4  fix-ppc-atomics.patch
856ae98e9e8670801827c3bd793dc14ed2c62c37365f8d04b452d7e1ab97300a0bf18c59b52ea686c2689d53aeed8e29e2c55207d3d4fb1fd8fc7fc820f33157  posix-runtest.patch
91b663f802aea9a473195940d3bf2ce3ca2af4e5b6e61a2d28ebbfe502ef2c764b574b7e87c49e60345d1a5d6b73d12920924c93b26be110c2ce824023347b6f  redis.initd
6d17d169b40a7e23a0a2894eff0f3e2fe8e4461b36f2a9d45468f0abd84ea1035d679b4c0a34029bce093147f9c7bb697e843c113c17769d38c934d4a78a5848  redis.logrotate
d87aad6185300c99cc9b6a478c83bf62c450fb2c225592d74cc43a3adb93e19d8d2a42cc279907b385aa73a7b9c77b66828dbfb001009edc16a604abb2087e99  redis.confd"
