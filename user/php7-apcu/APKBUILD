# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=php7-apcu
_pkgname=${pkgname#php7-}
pkgver=5.1.17
pkgrel=0
pkgdesc="Userland cache for PHP"
url="https://pecl.php.net/package/APCu"
arch="all"
license="PHP-3.01"
depends="php7"
makedepends="php7-dev"
subpackages=""
source="https://pecl.php.net/get/$_pkgname-$pkgver.tgz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	phpize
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--enable-apcu
	make
}

check() {
	# The tests have a prompt at the end for sending stats
	make NO_INTERACTION=1 test
}

package() {
	# It didn't listen to DESTDIR
	make INSTALL_ROOT="$pkgdir" install
}

sha512sums="7f6a6cd927ed4ff251497c2c79e8d832d1a44ddd59abdb175886d3c12ea27f26142e6b851ac5bb5acb49a0e3a998e9741ba0ff03310f51b9cd188cdf6cf12793  apcu-5.1.17.tgz"
