# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kst
pkgver=2.0.8
pkgrel=0
pkgdesc="Real-time dataset viewing and plotting tool"
url="https://kst-plot.kde.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev gsl-dev"
subpackages="$pkgname-doc $pkgname-lang"
langdir="/usr/share/kst/locale"
source="https://downloads.sourceforge.net/project/kst/Kst%20$pkgver/Kst-$pkgver.tar.gz
	kst-gsl2.patch"
builddir="$srcdir/Kst-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-Dkst_qt5="YesPlease" \
		-Dkst_install_prefix="/usr" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="81be1e9ef189dbf087c5626b984297d4c8f84f22fce50c4933ea467a65f8595c0254af78d55f2b2e324934346f0758b7a0cf48d06d1108026b60b7ffba1221fb  Kst-2.0.8.tar.gz
1dbd94d7e4298d768ab1e9e020dd207b848e72b30db6b1f2ef2f3eb0d528ec5c0a120593e7e7c06f4cb1e59e950a965e217c30564a26510a180d82418628d4e5  kst-gsl2.patch"
