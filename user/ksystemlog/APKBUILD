# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksystemlog
pkgver=19.08.1
pkgrel=0
pkgdesc="Friendly, powerful system log viewer tool"
url="https://www.kde.org/applications/system/ksystemlog/"
arch="all"
options="!check"  # Tests require X11
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kxmlgui-dev kconfig-dev
	kcoreaddons-dev kwidgetsaddons-dev kitemviews-dev kio-dev karchive-dev
	kdoctools-dev ki18n-dev kcompletion-dev ktextwidgets-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/ksystemlog-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="518dc5b77f31e4200decb1c011d7bb09e4e3c92871ada3c47a43e1df2b434c1f48ce0c467b5ac7cea9958a84b83de6b691a19a0a29ff4d530706d189540ea2f5  ksystemlog-19.08.1.tar.xz"
