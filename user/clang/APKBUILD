# Contributor Travis Tilley <ttilley@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=clang
# Note: Update together with llvm.
pkgver=8.0.1
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="A C language family front-end for LLVM"
arch="all"
options="!dbg"
url="https://llvm.org/"
license="NCSA"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="cmake isl-dev libedit-dev libexecinfo-dev libxml2-dev libxml2-utils
	llvm-dev>=$_llvmver llvm-static>=$_llvmver llvm-test-utils>=$_llvmver
	z3 z3-dev"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-libs
	$pkgname-analyzer::noarch"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/cfe-$pkgver.src.tar.xz
	0001-Add-support-for-Ad-lie-Linux.patch
	0008-Fix-ClangConfig-cmake-LLVM-path.patch
	cfe-005-ppc64-dynamic-linker-path.patch
	pmmx-musl.patch
	ppc64-elfv2.patch
	secure-plt.patch
	use-llvm-lit.patch
	"
builddir="$srcdir/cfe-$pkgver.src"

build() {
	CMAKE_PREFIX_PATH=/usr/lib/llvm$_llvmver/lib/cmake \
	cmake -Wno-dev \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_VERBOSE_MAKEFILE=OFF \
		-DCLANG_VENDOR=${DISTRO_SHORT_NAME:-Adélie} \
		-DCLANG_BUILD_EXAMPLES=OFF \
		-DCLANG_INCLUDE_DOCS=ON \
		-DCLANG_INCLUDE_TESTS=ON \
		-DCLANG_PLUGIN_SUPPORT=ON \
		-DLIBCLANG_BUILD_STATIC=ON \
		-DLLVM_ENABLE_EH=ON \
		-DLLVM_ENABLE_RTTI=ON \
		-Bbuild \
		.

	make -C build clang-tblgen
	# too memory hungry
	if [ -z "$JOBS" ] || [ $JOBS -gt 32 ]; then
		make -C build -j32
	else
		make -C build
	fi
}

check() {
	[ -f build/bin/llvm-lit ] || ln -s /usr/bin/lit build/bin/llvm-lit
	make -C build check-clang
}

package() {
	local _dir _file

	make DESTDIR="$pkgdir" -C build install
	install -m 644 build/lib/libclang.a "$pkgdir"/usr/lib

	# mozilla will never be happy
	mkdir -p "$pkgdir"/usr/lib/llvm$_llvmver/bin
	mkdir -p "$pkgdir"/usr/lib/llvm$_llvmver/lib
	for _dir in bin lib; do
		cd "$pkgdir"/usr/$_dir
		for _file in *; do
			[ -f $_file ] || continue
			ln -s "../../../$_dir/$_file" \
				"$pkgdir"/usr/lib/llvm$_llvmver/$_dir/$_file
		done
		cd "$OLDPWD"
	done

	# needed for at least Qt Creator
	mkdir -p "$pkgdir"/usr/include/llvm$_llvmver
	ln -s ../clang "$pkgdir"/usr/include/llvm$_llvmver/clang
	ln -s ../clang-c "$pkgdir"/usr/include/llvm$_llvmver/clang-c
}

static() {
	pkgdesc="Static libraries for clang"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
	strip -d "$subpkgdir"/usr/lib/*.a
}

analyzer() {
	pkgdesc="Clang source code analysis framework"
	depends="$pkgname=$pkgver-r$pkgrel perl python3"

	cd "$pkgdir"

	mkdir -p "$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/libexec \
		"$subpkgdir"/usr/share/
	mv usr/bin/scan-* "$subpkgdir"/usr/bin/
	mv usr/libexec/*-analyzer "$subpkgdir"/usr/libexec/
	mv usr/share/scan-* "$subpkgdir"/usr/share/
}

sha512sums="1227b2d32052c70b4b494659849000188fce46fc31a71f3352ba8457ac0b0b17e4bc7c8589874c8586d55aa808ee6c1fceb7df501aafa33599f8df7bfd2b791d  cfe-8.0.1.src.tar.xz
15f8c4e3453c0620ccd47e5dae37de29f722bc5f16aa64a3ff1ac2a2d522dd3e13dada539f1944c51f334d0f91ac60d0189578c252c49ba39d17cdf42021086b  0001-Add-support-for-Ad-lie-Linux.patch
9485fe4fd6182df543735ed8f4ce618693d0faeafa86d3f9574a6c7abf50978e2d56e0a94be3ed94d515cc937c388d66ceff1bbc9bb120d371b6d3e95340da00  0008-Fix-ClangConfig-cmake-LLVM-path.patch
ee5606b130354983202a1d7c96183ea81dec1048a92dc059bbc07fd4e8e21c1279064422d8f432d2b6e5d88d883f1e2c8f8dada716d82ae4068c9fea3e1b0a54  cfe-005-ppc64-dynamic-linker-path.patch
13b6b03798c724f1444d8639cfab0c971560618ff7a61f461bde5ab004bdbadbf9960ab30bbb19fcc9e305712b66a22bf88998f2cdd6f5e3ba7065539abc40a0  pmmx-musl.patch
de0b806ad632c76789f7cf750e6b23cc72b3708a9566ef25d5ac3f51840972163a57304802259a0408fb33f44334ec12cef49600f766e8329af489c9a991af74  ppc64-elfv2.patch
08a231b73561c78195a717aed022c60264b3d77ad01df07835fec397994b09cf703fe16ad7a8f62acdc37269e0a6ef8bd07c3289db58490440c766f58c3af239  secure-plt.patch
1b0e7fe72de58a5a22d4c4afe7c3bcab23244e6e5e01df43752a38b4cafcfb47ba1877641db285cd31869d5521657b6c6280506d872c25191b10a8661b43b0fe  use-llvm-lit.patch"
