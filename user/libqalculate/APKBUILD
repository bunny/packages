# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libqalculate
pkgver=2.8.1
pkgrel=0
pkgdesc="Library implementing a powerful, versatile desktop calculator"
url="https://qalculate.github.io/"
arch="all"
options="!check"  # missing potfiles
license="GPL-2.0+"
depends=""
depends_dev="gmp-dev mpfr-dev"
makedepends="$depends_dev curl-dev icu-dev intltool libxml2-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang qalc"
source="https://github.com/Qalculate/libqalculate/releases/download/v$pkgver/libqalculate-$pkgver.tar.gz
	"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

qalc() {
	pkgdesc="Powerful, versatile desktop calculator (terminal UI)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="a258bf4f67db70263626ac77822afc75fd7a9e5af2feb1930314daff219865b28c43e82fef6ceae4f3f23957447d6eaebdc709e606d28457bca0f47eefb9274a  libqalculate-2.8.1.tar.gz"
