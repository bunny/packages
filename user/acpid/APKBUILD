# Maintainer:
# Contributor: Morten Linderud <foxboron@archlinux.org>
# Contributor: Sébastien Luttringer
# Contributor: xduugu
# Contributor: Manolis Tzanidakis
# Contributor: Jonathan Schmidt <j.schmidt@archlinux.us>
# Contributor: multiplexd <multi@in-addr.xyz>

pkgname=acpid
pkgver=2.0.32
pkgrel=0
pkgdesc="Daemon for handling ACPI power management events"
url="https://sourceforge.net/projects/acpid2/"
arch="all"
options="!check" # No test suite.
license="GPL-2.0+ AND GPL-2.0-only"
depends=""
makedepends=""
subpackages="$pkgname-doc $pkgname-openrc"
source="http://downloads.sourceforge.net/sourceforge/acpid2/$pkgname-$pkgver.tar.xz
	handler.sh
	default
	acpid.initd
	acpid.confd"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sbindir=/usr/sbin \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	make DESTDIR="$pkgdir" install

	install -m 755 -D "$srcdir"/acpid.initd \
		"$pkgdir"/etc/init.d/acpid
	install -m 644 -D "$srcdir"/acpid.confd \
		"$pkgdir"/etc/conf.d/acpid
	install -m 644 -D "$srcdir"/default \
		"$pkgdir"/etc/acpi/events/default
	install -m 755 -D "$srcdir"/handler.sh \
		"$pkgdir"/etc/acpi/handler.sh
}

sha512sums="c7afffdf9818504e1ac03b0ad693a05f772bfd07af9808262b3b6bb82ca4dabe6253c94e6dc59e5be6f0da9e815e8bcf2d3e16f02b23d0248b6bad4509e78be7  acpid-2.0.32.tar.xz
42e8a66c4fd4de3bf0b6adc359c5d5a08e45bf38fc2945c75ab0bf38058c10906e2c84f50895bb85b38e1a19646d91e40a875f2b45f67e07a9336d27bfa2dcd8  handler.sh
2ca236168ce6aaa56c980568c781d6e51590870b7a7936c74bf72532ef3f6c60a369f37597202f3a236d60637fd0daa6611d4ae0348484011ff71871a9914246  default
7381d30b5c6478cdbf5dff93ae95baa0b3b1fe0a04b02cf491831f1657d6f71b8eef121b7e78f3201d11a5856bfb30df0a57437c76e6fbe05ad88cd45e86ae64  acpid.initd
518cb397b2aa63b893ead1be08f32fe436d19b72663dee66834cfbc112b003333f0df8b9e4f1ffe64b813783f657d3fe8b9a0c5e992d5665583357e68b266705  acpid.confd"
