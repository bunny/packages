# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-framework
pkgver=5.54.0
pkgrel=0
pkgdesc="Frameworks for the KDE Plasma 5 desktop environment"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires accelerated X11 *and* system DBus running.
license="LGPL-2.1+ AND GPL-2.0+"
depends="qt5-qtquickcontrols"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kconfig-dev kcoreaddons-dev
	kpackage-dev kservice-dev kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen libx11-dev
	libxcb-dev graphviz qt5-qtsvg-dev qt5-qttools-dev kdoctools-dev kio-dev
	kactivities-dev karchive-dev kconfigwidgets-dev kdbusaddons-dev
	kdeclarative-dev kglobalaccel-dev kguiaddons-dev kiconthemes-dev
	kirigami2-dev ki18n-dev knotifications-dev kwayland-dev
	qt5-qtquickcontrols2-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/plasma-framework-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="618f2beb3b8a25cacf7c11339ba15cae10db461c61e278a5ba845e9afac7e9edb48fde16bd78b4bb7d16c07001f8d9fbdbf5bfd58593488a544fa183b3df8c15  plasma-framework-5.54.0.tar.xz"
