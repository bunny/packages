# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=okteta
pkgver=0.26.2
pkgrel=0
pkgdesc="Graphical hex/binary editor"
url="https://www.kde.org/applications/utilities/okteta/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev kio-dev
	kbookmarks-dev kcodecs-dev kcompletion-dev kconfigwidgets-dev ki18n-dev
	kcrash-dev kdbusaddons-dev kdoctools-dev kiconthemes-dev kcmutils-dev
	knewstuff-dev kparts-dev kservice-dev kwidgetsaddons-dev kxmlgui-dev
	qca-dev shared-mime-info qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/okteta/$pkgver/src/okteta-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	local _skip="libkasten-core-testdocumenttest|libkasten-core-documentmanagertest|oktetakasten-document-bytearraydocumenttest|kpart-oktetaparttest|replacejobtest"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "($_skip)"
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9d79cfecf80a7bfd4eb42ac8ee93d83140064601c16be0bed38481f7f0ea8ed2fa0aedfd16be3547f2bbd65ab483281e0ecdc0e59799040d67ecf1b4cda39246  okteta-0.26.2.tar.xz"
