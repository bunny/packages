# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmix
pkgver=19.08.1
pkgrel=0
pkgdesc="KDE sound mixer"
url="https://www.kde.org/applications/multimedia/kmix/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcompletion-dev ki18n-dev
	kconfig-dev kconfigwidgets-dev kcrash-dev kdbusaddons-dev kdoctools-dev
	kglobalaccel-dev kiconthemes-dev kinit-dev knotifications-dev solid-dev
	plasma-framework-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev
	alsa-lib-dev libcanberra-dev pulseaudio-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kmix-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="84e3224bcb9fed06e2a8f0dd61c85e8684579d89078114a5ef2b6fc5f740598d05d0ad20dc7df0a77473d47b265a5bd49f12f3630911fa8bf294cae6b0e51481  kmix-19.08.1.tar.xz"
