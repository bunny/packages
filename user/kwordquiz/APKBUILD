# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwordquiz
pkgver=19.08.1
pkgrel=0
pkgdesc="Flash card trainer for KDE"
url="https://www.kde.org/applications/education/kwordquiz/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev kcrash-dev
	kconfig-dev kconfigwidgets-dev kdoctools-dev kguiaddons-dev
	kiconthemes-dev kitemviews-dev knotifyconfig-dev knewstuff-dev
	knotifications-dev kxmlgui-dev kdelibs4support-dev
	libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kwordquiz-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="f81b2b2d8f3abaefe91eb028ddbb702da8a663ddf01941c5e927cda5f2f473b73192b850a7349c18b1bac79ea835da0bf96e90714124604a50aa58410543c962  kwordquiz-19.08.1.tar.xz"
