# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-config
pkgver=0.14.1
pkgrel=0
pkgdesc="Collection of tools for configuring LXQt and the underlying system"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtsvg-dev qt5-qttools-dev
	liblxqt-dev>=${pkgver%.*}.0 lxqt-build-tools>=0.6.0 libxcursor-dev eudev-dev
	kwindowsystem-dev libkscreen-dev xf86-input-libinput-dev libxi-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-config/releases/download/$pkgver/lxqt-config-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make -j1
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="747644f8e0623cb2e8eadda57c7307387423152a089d5b423b06650fbf235371764a26e82363cbee9db5c1145e3fdb754e6ecdcf0930dccec375dda424c8b13f  lxqt-config-0.14.1.tar.xz"
