# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkdcraw
pkgver=19.08.1
pkgrel=0
pkgdesc="RAW image file format support for KDE"
url="https://www.KDE.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules libraw-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/applications/$pkgver/src/libkdcraw-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="90ff0c3a9719df4ae97dc401592adc29cdbde0bd7aabe13b468cfc3d8045d50b20d670903274f7960039ca6a4c912dc877b314ed160d07fb8b5b96912a0bf115  libkdcraw-19.08.1.tar.xz"
