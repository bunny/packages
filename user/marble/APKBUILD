# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=marble
pkgver=19.08.1
pkgrel=0
pkgdesc="Free, open-source map and virtual globe"
url="https://marble.kde.org/"
arch="all"
options="!check"  # Test suite requires package to be already installed.
license="LGPL-2.1-only AND GPL-2.0-only"
depends="shared-mime-info"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev zlib-dev
	qt5-qtserialport-dev krunner-dev kcoreaddons-dev kwallet-dev knewstuff-dev
	kio-dev kparts-dev kcrash-dev ki18n-dev phonon-dev plasma-framework-dev
	qt5-qtpositioning-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://download.kde.org/stable/applications/$pkgver/src/marble-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4488ae8a99d999dd17ba6d73cb88b84dab509e70a027b87c22948602fb9b04bacedaa448ea1bd1234a076373bd3fa5ea65d5e738e7febecf7aa75f6c7a0c6890  marble-19.08.1.tar.xz"
