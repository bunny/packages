# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=xfconf
pkgver=4.14.1
pkgrel=0
pkgdesc="Configuration framework for the XFCE desktop environment"
url="https://xfce.org"
arch="all"
options="!check" # tests require X11
license="LGPL-2.1+ AND GPL-2.0+"
depends="dbus"
makedepends="intltool gtk+3.0-dev libxfce4util-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://archive.xfce.org/src/xfce/xfconf/4.14/xfconf-$pkgver.tar.bz2"

build() {
	LIBS="-lintl" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

perl() {
	depends="perl perl-glib"
	pkgdesc="Perl bindings for xfconf"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/perl5 "$subpkgdir"/usr/lib
}

sha512sums="727d013f7e71e0eb9ff17e464acd65c1fa507f8eba1bd29621dae070161042f60f47b8ac048b28849bf45232d04d5d6e48530aa0bd1e661a3f58db47703a8808  xfconf-4.14.1.tar.bz2"
