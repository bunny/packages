# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi
pkgver=18.12.3
pkgrel=1
pkgdesc="Libraries and storage system for PIM data"
url="https://community.kde.org/KDE_PIM/Akonadi"
arch="all"
options="!check"  # Test suite requires running D-Bus session.
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt5-qtbase-postgresql"
depends_dev="qt5-qtbase-dev boost-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kio-dev kitemmodels-dev libxml2-dev"
makedepends="$depends_dev cmake extra-cmake-modules libxslt-dev qt5-qttools-dev
	libxslt-dev shared-mime-info sqlite-dev kcompletion-dev kcrash-dev
	kdesignerplugin-dev ki18n-dev kiconthemes-dev kitemviews-dev kio-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/akonadi-$pkgver.tar.xz
	akonadiserverrc
	attributes.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/config/akonadi
	install -m 644 "$srcdir"/akonadiserverrc \
		"$pkgdir"/usr/share/config/akonadi
}

sha512sums="f97e716be612001a8176f20afbe80a1cf4c78c6d348d92e542307ea951e1ff74189d2f0847675ed44ad65845b52a5180346b3866f5a8d82b55ed613d107ac346  akonadi-18.12.3.tar.xz
b0c333508da8ba5c447827b2bad5f36e3dc72bef8303b1526043b09c75d3055790908ac9cbb871e61319cfd4b405f4662d62d2d347e563c9956f4c8159fca9ab  akonadiserverrc
63a26cf5c20583a372146d11050f5003242997743fe9470fb2b7556d5aebb58c06dfeefc0c14c86a435f7172126dac0d52b1701c6bd872f10531bf1d99625e93  attributes.patch"
