# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpackage
pkgver=5.54.0
pkgrel=0
pkgdesc="Frameworks for managing KDE data packages"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires Plasma to be installed, causing circular dep
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev ki18n-dev kcoreaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 kdoctools-dev
	qt5-qttools-dev doxygen graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpackage-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="d26f5dad3634a442d55bae94903ec6d781fa1984a780eec4b1f9ea0f9d3ae50b0defd3ddb078d7b0410626250ded1ee1e5ca1f5ea7ecee1ede4a0ef30463a8c8  kpackage-5.54.0.tar.xz"
