# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=mac-fdisk
pkgver=0.1.16
_pkgver=0.1
pkgrel=2
pkgdesc="68K and PowerPC fixed disk partitioning utility"
url="https://adelielinux.org/"
arch="all !aarch64"  # Literally does not support this arch.
license="GPL-2.0"
options="!check"  # No test suite.
depends=""
makedepends="linux-headers"
subpackages="$pkgname-doc"
source="https://distfiles.adelielinux.org/source/mac-fdisk_0.1.orig.tar.gz
	mac-fdisk-0.1-debian.patch
	mac-fdisk-0.1-headers.patch
	mac-fdisk-0.1-more-arches.patch
	mac-fdisk-0.1_p16-ppc64.patch
	mac-fdisk-0.1_p16-proper-inline.patch
	mac-fdisk-amd64.patch
	mac-fdisk-fdisk-header-musl.patch
	mac-fdisk-large-disk-support.patch
	mac-fdisk-largerthan2gb.patch
	mac-fdisk-non-glibc-support.patch
	flush-stdout.patch"
builddir="$srcdir/$pkgname-$_pkgver.orig"

build() {
	cd "$builddir"
	make CFLAGS="-D_GNU_SOURCE"
}

package() {
	cd "$builddir"
	mkdir -p "$pkgdir"/sbin
	make DESTDIR="$pkgdir" install

	install -D -m644 "$builddir"/mac-fdisk.8.in \
		"$pkgdir"/usr/share/man/man8/mac-fdisk.8
	install -m644 "$builddir"/pmac-fdisk.8.in \
		"$pkgdir"/usr/share/man/man8/pmac-fdisk.8
}

sha512sums="1263e60a18111162f5ef439b5f9615cef9de94e7836cb998782a6e6d3bcf92a69e49566b33c7330bdea05de5e6ca83f7920c6707f342ce515440160695f07120  mac-fdisk_0.1.orig.tar.gz
1d1073c4bbb6ff403130254d1ff8e1b045933c9b09e032a278bba47c50ccf1806d3c3280a818937e36942392da7b03091f00063d2bcf9f4f33cb57a8bb1166f0  mac-fdisk-0.1-debian.patch
7d16300c02b293de87101e3ef83a8b7778d15123c261d562f0129704b74d0c3d7ca18730c52d37b599ff57c0b70dacd41aeefe99cf235aea4abd4b569e3d601e  mac-fdisk-0.1-headers.patch
ddee905f118d4ffa2bae0c849470f45d31baa955ae344241496863f742f7b5e00a2d458e20542138be53472f4b396461ffbcd724e98c10ca155ef38c2d39f94b  mac-fdisk-0.1-more-arches.patch
e521009d628934fe3925085c941d2bb3fba9c3322ccaaa6dd650ecbc6b990056f9bf186adb3a645dc8593efdb44611b69a7b6bf4e87d262870bbb515581e0ef3  mac-fdisk-0.1_p16-ppc64.patch
73b28195539a3e71b37e955f5f1e5cdc566ecf1c43a235b7689df235d84dec9d3807d3b5b49a506f758d7a1ff528388052bebfea58916381f2f0045eee350bac  mac-fdisk-0.1_p16-proper-inline.patch
162c5036e404519e57b63e1aee440bec4edca7fb7348efe985feeb37df9221d6829eaa601c8feb52c1ffd99cb8daf037dc93e161bb23c96927a65dd5e8b186d4  mac-fdisk-amd64.patch
d558d66fb91d845e09cca7c3832d2ec078581ed09574fb38d9476f0417901794ae70e4a284379977a5c50599ce9570da8359e321de91f9f77263601c090e501d  mac-fdisk-fdisk-header-musl.patch
c9abc952f1ce618b085a134145fa99efa9c10b55f3fa773fb6b74a66468537bc3567ec632fe6e464c5158383135c858c171d8d94d7142d38beff39a17e31187a  mac-fdisk-large-disk-support.patch
b5988eb6aa64324421398461396b0fdd0b55d7643094c042bea7264ec0f9913e8a793f3f550c15928419e697a14f2932f483c4b98a54618af99b35aa7ceae757  mac-fdisk-largerthan2gb.patch
f9d988ae693707b7b9bcf3cbaba4f65d1337c85e733209b146154bca4246021d2277824cc4766d66e9594d9fac49c0c0216f14c24511b6ad5104ac0833d9975b  mac-fdisk-non-glibc-support.patch
e036a8b3538550fcc55b8e2a4dd1d53f035f4c43f5aa919fde51df4f8431f5f2b3b4b9b6b6b589adf575b4b012ce48d55430cf2fd47e14d57182ff3ddb443ecd  flush-stdout.patch"
