# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer:
pkgname=mlt
pkgver=6.16.0
pkgrel=0
pkgdesc="MLT multimedia framework"
url="https://www.mltframework.org/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+ AND GPL-3.0+"
makedepends="bsd-compat-headers ffmpeg-dev fftw-dev frei0r-plugins-dev
	gtk+2.0-dev libexif-dev libsamplerate-dev libxml2-dev sdl-dev sdl2-dev
	qt5-qtbase-dev qt5-qtsvg-dev sdl_image-dev sox-dev"
subpackages="$pkgname-dev"
source="https://github.com/mltframework/mlt/releases/download/v$pkgver/mlt-$pkgver.tar.gz
	mlt-6.14.0-locale-header.patch
	"

build() {
	local _maybe_asm=""

	case $CTARGET_ARCH in
		pmmx)  _maybe_asm="--disable-sse --target-arch=i586" ;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--avformat-swscale \
		--enable-motion-est \
		--enable-gpl \
		--enable-gpl3 \
		--disable-rtaudio \
		$_maybe_asm
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="554e8b9baa7a8578cc52315fe0583c61762bf6fbbcdd4a1e4f25753846d92f013e7d74745498625fcc781de993aa0526fd761920450b4314e67105783b9bde26  mlt-6.16.0.tar.gz
d00f01d50d5c78b1da5b43dc2b0bbfc49d5e383b602169ae9554734d29f6d43b9da8f97546141933c06ff0367bb4c9f0d2161bbcb6f016265bb0aa1dcbfcb3b1  mlt-6.14.0-locale-header.patch"
