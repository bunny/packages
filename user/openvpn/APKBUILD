# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Lee Starnes <lee@canned-death.us>
pkgname=openvpn
pkgver=2.4.7
pkgrel=0
pkgdesc="A robust, and highly configurable VPN (Virtual Private Network)"
url="https://openvpn.net/"
arch="all"
license="GPL-2.0-only WITH openvpn-openssl-exception"
subpackages="$pkgname-doc $pkgname-dev $pkgname-auth-pam:pam $pkgname-openrc"
depends="iproute2"
makedepends="openssl-dev lzo-dev linux-pam-dev linux-headers"
install="$pkgname.pre-install"
source="https://swupdate.openvpn.net/community/releases/$pkgname-$pkgver.tar.gz
	openvpn.initd
	openvpn.confd
	openvpn.up
	openvpn.down
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--sysconfdir=/etc/openvpn \
		--enable-crypto \
		--enable-iproute2
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	# install samples
	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/samples
	cp -a sample/sample-* "$pkgdir"/usr/share/doc/$pkgname/samples
	install -D -m644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

	# install init.d and conf.d
	install -Dm755 "$srcdir"/openvpn.initd "$pkgdir"/etc/init.d/openvpn
	install -Dm644 "$srcdir"/openvpn.confd "$pkgdir"/etc/conf.d/openvpn

	# install up and down scripts
	install -Dm755 "$srcdir"/openvpn.up "$pkgdir"/etc/openvpn/up.sh
	install -Dm755 "$srcdir"/openvpn.down "$pkgdir"/etc/openvpn/down.sh
}

pam() {
	pkgdesc="OpenVPN plugin for PAM authentication"
	mkdir -p "$subpkgdir"/usr/lib/openvpn/plugins
	mv "$pkgdir"/usr/lib/openvpn/plugins/*-auth-pam* \
		"$subpkgdir"/usr/lib/openvpn/plugins/
}

sha512sums="c96359ceffe7e47dd0881d4b37dfef45a9ef4450fcfcd9b95de70c73fb0b5756463f3e0dd3e4087643e00b0aeb72b99c5ce9c6f38edbbb491d0fd5a426f65972  openvpn-2.4.7.tar.gz
3594937d4cc9d7b87ac6a3af433f651ed9695f41586994f9d9789554fbe3f87f054b997b89486eda4ae0b852d816aac9007222168d585910aa9f255073324bd9  openvpn.initd
6b2353aca9df7f43044e4e37990491b4ba077e259ebe13b8f2eb43e35ca7a617c1a65c5bfb8ab05e87cf12c4444184ae064f01f9abbb3c023dbbc07ff3f9c84e  openvpn.confd
cdb73c9a5b1eb56e9cbd29955d94297ce5a87079419cd626d6a0b6680d88cbf310735a53f794886df02030b687eaea553c7c569a8ea1282a149441add1c65760  openvpn.up
4456880d5c2db061219ba94e4052786700efa5e685f03b0d12d75a6023e3c0fc7b5242cc3d2bd3988e42fcd99701ab13a6257b1a0943b812318d30c64843ad27  openvpn.down"
