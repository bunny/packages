# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=ruby-rspec-support
_gemname=${pkgname#ruby-}
pkgver=3.8.3
pkgrel=0
pkgdesc="Support utilities for RSpec gems"
url="https://rubygems.org/gems/rspec-support"
arch="noarch"
options="!check"  # rspec's tests are written in rspec
license="MIT"
depends="ruby"
source="$pkgname-$pkgver.tar.gz::https://github.com/rspec/$_gemname/archive/v$pkgver.tar.gz
	gemspec.patch"
builddir="$srcdir/$_gemname-$pkgver"

build() {
	cd "$builddir"
	gem build $_gemname.gemspec
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"

	cd "$builddir"
	gem install --local \
		--install-dir "$gemdir" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Clean-up...
	rm -r "$gemdir"/cache \
		"$gemdir"/build_info \
		"$gemdir"/doc
}

sha512sums="90d678e08ca93be30ace986a60aba4c4a81a58d7c56d34863aca70f91efe92480299970aa7eb158e3f8c4878b1ea3303aa0afabfc8c7782bc78f7f63b90b4f9a  ruby-rspec-support-3.8.3.tar.gz
e9d611ea1789e835f742aa92f1e668840139e2621898edf158dc53e111db4119a324da65d2d28f5c6e737c82f261f4adb3beb8c244ee01d2f618778ed62d3731  gemspec.patch"
