# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=powerdevil
pkgver=5.12.8
pkgrel=0
pkgdesc="KDE Plasma power management utilities"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	kactivities-dev kauth-dev kconfig-dev kdbusaddons-dev kglobalaccel-dev
	ki18n-dev kidletime-dev kio-dev knotifyconfig-dev kdelibs4support-dev
	kwayland-dev libkscreen-dev plasma-workspace-dev solid-dev eudev-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/powerdevil-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ad159287ababd4410d2277bc43c0f006209f457a1c08ba903db25f60c62be95fd3b114abd56d5c64ebaccbecb523338766bc28468dc54fa7fc072f6381b8e181  powerdevil-5.12.8.tar.xz"
