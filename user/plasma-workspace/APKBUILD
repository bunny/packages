# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=plasma-workspace
pkgver=5.12.8
pkgrel=0
pkgdesc="KDE Plasma 5 workspace"
url="https://www.kde.org/plasma-desktop"
arch="all"
options="!check"  # Test requires X11 accelration.
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0+ AND MIT AND LGPL-2.1-only AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
# startkde shell script calls
depends="kinit qdbus qtpaths xmessage xprop xset xsetroot"
# font installation stuff
depends="$depends mkfontdir"
# QML deps
depends="$depends qt5-qtgraphicaleffects qt5-qtquickcontrols solid"
# other runtime deps / plugins
depends="$depends libdbusmenu-qt kcmutils kde-cli-tools kded kdesu kio-extras
	ksysguard kwin milou plasma-integration pulseaudio-utils iso-codes xrdb"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kdelibs4support-dev
	kitemmodels-dev kservice-dev kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtscript-dev
	iso-codes-dev libdbusmenu-qt-dev libxtst-dev xcb-util-image-dev

	baloo-dev kactivities-dev kcmutils-dev kcrash-dev kdbusaddons-dev
	kdeclarative-dev kdesu-dev kdoctools-dev kglobalaccel-dev kholidays-dev
	kidletime-dev kjsembed-dev knewstuff-dev knotifyconfig-dev kpackage-dev
	krunner-dev kscreenlocker-dev ktexteditor-dev ktextwidgets-dev
	kwallet-dev kwayland-dev kwin-dev kxmlrpcclient-dev libksysguard-dev
	plasma-framework-dev prison-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz
	cmake-fixes.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c19d338117c5d9b05581fc1be7d99fc60f4453a009a04ddf3609add1fef7ad08bdd0d0c19c0f4151484253036cc444d8301c3fb8ee0d29d8ecc19d9929a9ff8b  plasma-workspace-5.12.8.tar.xz
39ea169f50b98fd2dd246396133a3e8d9c844409338c9c3ecd79474945377886c5d1050205ad73c3d8c789315e8a4353311bafa9f1ddd22f25403b3ed7f30fde  cmake-fixes.patch"
