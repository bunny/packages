# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gst-plugins-good
pkgver=1.16.1
pkgrel=0
pkgdesc="GStreamer multimedia framework - Free, well-written plugins"
url="https://gstreamer.freedesktop.org/modules/gst-plugins-good.html"
arch="all"
license="LGPL-2.1+"
depends=""
checkdepends="orc-compiler cmd:which"
makedepends="gst-plugins-base-dev gstreamer-dev gobject-introspection-dev
	orc-dev libgudev-dev v4l-utils-dev libx11-dev cairo-dev flac-dev
	gdk-pixbuf-dev gtk+3.0-dev libjpeg-turbo-dev lame-dev libdv-dev
	libpng-dev mpg123-dev pulseaudio-dev libraw1394-dev libiec61883-dev
	libavc1394-dev libsoup-dev speex-dev taglib-dev wavpack-dev zlib-dev
	bzip2-dev"
subpackages="$pkgname-doc $pkgname-gtk $pkgname-lang"
ldpath="/usr/lib/gstreamer-1.0"
source="https://gstreamer.freedesktop.org/src/gst-plugins-good/gst-plugins-good-$pkgver.tar.xz
	endian-test.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-introspection \
		--with-package-origin="${DISTRO_NAME:-Adélie Linux} (${DISTRO_URL:-https://www.adelielinux.org/})"
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

gtk() {
	pkgdesc="$pkgdesc (Gtk+ 3 integration)"
	install_if="$pkgname=$pkgver-r$pkgrel gtk+3.0"
	mkdir -p "$subpkgdir"/usr/lib/gstreamer-1.0
	mv "$pkgdir"/usr/lib/gstreamer-1.0/libgstgdkpixbuf.so \
		"$subpkgdir"/usr/lib/gstreamer-1.0/
	mv "$pkgdir"/usr/lib/gstreamer-1.0/libgstgtk.so \
		"$subpkgdir"/usr/lib/gstreamer-1.0/
}

sha512sums="06dc1887626864c30248df53d48e85ffdf0d1660d74f70f23c325c0844e9ca390556846cbae77fe9a4a90cb992e9a55117c15a9031b44a48d45dc07a61b18da4  gst-plugins-good-1.16.1.tar.xz
1cecf260cfeb19675a819f9858426aaed3b9627ed90b8facb6cb12dfc0b265232639b1888d7cc03edb87025e3eddab6e3606f16b5f7ca261a6b1c39b0dbdc6f7  endian-test.patch"
