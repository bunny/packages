# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=phonon
pkgver=4.11.1
pkgrel=0
pkgdesc="Qt library for playing multimedia files"
url="https://phonon.kde.org/"
arch="all"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
makedepends="cmake extra-cmake-modules pulseaudio-dev
	qt5-qtbase-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-designer"
source="https://download.kde.org/stable/phonon/$pkgver/phonon-$pkgver.tar.xz
	ecm.patch
	qt59.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DPHONON_INSTALL_QT_EXTENSIONS_INTO_SYSTEM_QT=TRUE \
		-DWITH_GLIB2=TRUE \
		-DWITH_PulseAudio=TRUE \
		-DPHONON_BUILD_PHONON4QT5=TRUE \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

designer() {
	pkgdesc="$pkgdesc (Qt Designer plugin)"
	install_if="$pkgname=$pkgver-$pkgrel qt5-qttools"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/
	mv "$pkgdir"/usr/lib/qt5/plugins/designer \
		"$subpkgdir"/usr/lib/qt5/plugins/
	rmdir "$pkgdir"/usr/lib/qt5/plugins || true # Never mind
}

sha512sums="858b2b0d7b0336af76d226b30f3acd1914e7297e0879d5a417fa1b87b13c812f9aab7e20adcad33ce1a03624ce78323dd9968b4b277caf85f800ca60aa134f74  phonon-4.11.1.tar.xz
4de5a7ec143f7bf5068b376b77a1d8acb85225ee87d7ab59cb315ef59be3351305602354b8ef96a1a3fbb17308441704f2c2dfa2fcefadf0560708b4eb377e64  ecm.patch
06614abd2db7634acdf7bd27c026ea4544845bdd995b4bdeb9b1d2c9fa61940bdb839c39c18c4c9c487b95c2a91aae48056742ce2c8e558bd9186a941b8d0bac  qt59.patch"
