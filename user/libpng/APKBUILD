# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=libpng
pkgver=1.6.37
pkgrel=0
pkgdesc="Portable Network Graphics library"
url="http://www.libpng.org/"
arch="all"
license="Libpng"
depends=""
depends_dev=""
makedepends="autoconf automake libtool zlib-dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-utils"
source="https://downloads.sourceforge.net/$pkgname/$pkgname-$pkgver.tar.gz
	https://downloads.sourceforge.net/sourceforge/$pkgname-apng/$pkgname-$pkgver-apng.patch.gz
	libpng-fix-arm-neon.patch
	"

prepare() {
	cd "$builddir"
	gunzip -c "$srcdir"/$pkgname-$pkgver-apng.patch.gz | patch -p1
	default_prepare
	# libpng-fix-arm-neon.patch modifies configure.ac
	autoreconf -vif
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE
	rm -f "$pkgdir"/usr/lib/*.la
}

utils() {
	pkgdesc="$pkgdesc (pngfix utils)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="2ce2b855af307ca92a6e053f521f5d262c36eb836b4810cb53c809aa3ea2dcc08f834aee0ffd66137768a54397e28e92804534a74abb6fc9f6f3127f14c9c338  libpng-1.6.37.tar.gz
226adcb3a8c60f2267fe2976ab531329ae43c2603dab4d0cf8f16217d64069936b879f3d6516b75d259c47d6f5c5b1f24f887602206c8e46abde0fb7f5c7946b  libpng-1.6.37-apng.patch.gz
e3fae918f14bc34e7c126892f69527c6e1b4d72593835ece839d9a28cff55a886b2030f877cf9e29b2c97abf2e47bbb5ba54584828edd2a841c2556f330b9c7e  libpng-fix-arm-neon.patch"
