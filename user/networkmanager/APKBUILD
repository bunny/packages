# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=networkmanager
pkgver=1.20.4
pkgrel=0
pkgdesc="Network management daemon"
url="https://wiki.gnome.org/Projects/NetworkManager"
arch="all"
options="!check"  # Requires dbus-python and running DBus server.
license="GPL-2.0+ AND LGPL-2.1+"
depends="dhcpcd iputils ppp wpa_supplicant-dbus"
makedepends="bluez-dev consolekit2-dev curl-dev dbus-dev dbus-glib-dev
	eudev-dev glib-dev gobject-introspection-dev intltool libedit-dev
	libndp-dev libxslt modemmanager-dev ncurses-dev newt-dev nss-dev
	perl polkit-dev ppp-dev py3-pygobject util-linux-dev vala-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-openrc"
source="https://download.gnome.org/sources/NetworkManager/1.20/NetworkManager-$pkgver.tar.xz
	editline.patch
	errno.patch
	musl.patch
	qsort_r.patch
	random.patch
	tests.patch

	01-org.freedesktop.NetworkManager.rules
	10-openrc-status
	nm.confd
	nm.initd
	"
builddir="$srcdir/NetworkManager-$pkgver"

build() {
	# pppd plugin dir is a huge hack.
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-json-validation \
		--disable-more-warnings \
		--disable-ovs \
		--disable-qt \
		--disable-static \
		--enable-bluez5-dun \
		--enable-concheck \
		--enable-polkit=yes \
		--enable-ppp \
		--enable-vala \
		--with-crypto=nss \
		--with-dbus-sys-dir=/etc/dbus-1/system.d \
		--with-dhcpcd \
		--with-ebpf=yes \
		--with-iptables=/sbin/iptables \
		--with-nmcli=yes \
		--with-nmtui \
		--with-pppd-plugin-dir=/usr/lib/pppd/$(ls -1 /usr/lib/pppd | head) \
		--with-session-tracking=consolekit \
		--with-suspend-resume=consolekit \
		--without-dhclient \
		--without-libaudit \
		--without-libpsl \
		--without-netconfig \
		--without-systemd-journal
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -D -m755 "$srcdir"/nm.initd "$pkgdir"/etc/init.d/NetworkManager
	install -D -m644 "$srcdir"/nm.confd "$pkgdir"/etc/conf.d/NetworkManager
	install -D -m600 -t "$pkgdir"/usr/share/polkit-1/rules.d/ \
		"$srcdir"/01-org.freedesktop.NetworkManager.rules
	install -D -m755 -t "$pkgdir"/etc/NetworkManager/dispatcher.d/ \
		"$srcdir"/10-openrc-status
	install -d "$pkgdir"/etc/NetworkManager/system-connections
	touch "$pkgdir"/etc/NetworkManager/system-connections/.keepdir
}

openrc() {
	default_openrc
	mkdir -p "$subpkgdir"/etc/NetworkManager/dispatcher.d
	mv "$pkgdir"/etc/NetworkManager/dispatcher.d/10-openrc-status \
		"$subpkgdir"/etc/NetworkManager/dispatcher.d/
}

sha512sums="d08f7e8f5ea8f8567834a53a68ff7aeff09839658b73dc17b31dbdd7dd5c36b09f4e563c2ebc52d1e1574a84163e66e0bfee01c3762a222aa7154d18c09660cb  NetworkManager-1.20.4.tar.xz
a849f8172b88370f48217e04f2fafb4431db32415df52af7d4a388f604f7d03c4c21b6ad37afb57e52700a38a7e78365025da99991bc8d179c73dabb31e05256  editline.patch
0006d9c538a72673746b6aede8c61e0b7a23b055f8276fa2dde6e70f8c00d60854cf49678c996764f83b40cf06c5c2cd24c8c382d900aaf97fc840342e29500f  errno.patch
313e57823ffa49bd7b76355f8cea6932737ae4b38cb00eb183b12093a8109e079dc7439b2b35fa6bf9b83f2937729cca847a2bf31857382e9c3ae0b945c2dd6b  musl.patch
5142bf14ac5574a3ae4bc8753055c534702892871ca45a1bc4d48d4d5c07d11bd54e52861458a673c328d129cdd6f1f749a9546eba1c16fea5173f92840506de  qsort_r.patch
f149b9bb7b16c6e79685ff6a8c0f6f20a692bb675f66f8dd5dffe2158850b15d7a4591be8e906adec5e16047b681d0501b3228c4af5d218152ec8f82068fd414  random.patch
942986f22b1b522420afb885ca5c1d5dcaae10c193e001cb0604a78125b5efcc7f3e2b318f2b1f44b867369a243f7467bb00948617ac1d44271da7fd6e6b2ce6  tests.patch
22dfa5b071891ba16b9a8b90a1964d56e370cb640d78191934ae802e568f92c299b9d4d965f7081fb36a8c09378bfb0b33193b2332cbdc28c6799b152128c214  01-org.freedesktop.NetworkManager.rules
26f962cea0b6a75549d16c19a71e3559239b08b001927882702f5d9355a2cc95f7b13c581439f758a950d9a5bfb73b86ba61a5ffb22c9abe19f5b05fe5f5834a  10-openrc-status
f8c9331834cbc66ab0e4e6f4a120fde6a08a435d680a1e1087718fdbb2b9843a313e7ec30b7008822999dafd98e152aa7d2a96f87320ba9c91c3adb63e0b7c9a  nm.confd
a8356480592ec5e818b390b83cb258248d64850748e9e485bed68befabf067219d02fda137ee63684e0cd2c0e4f5fca30f94f8db71f811149183ff4715159658  nm.initd"
