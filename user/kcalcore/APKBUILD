# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalcore
pkgver=18.12.3
pkgrel=0
pkgdesc="Library for managing a calendar of events"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev attica-dev kparts-dev"
makedepends="$depends_dev cmake extra-cmake-modules bison libical-dev
	kconfig-dev kdelibs4support-dev"
checkdepends="tzdata"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/applications/$pkgver/src/kcalcore-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	TZ=UTC CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'Compat-libical3'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c347bfba365cb8d88fe11f743f5e5870e31a2d526f0cecda3bdebccadbf8e1ae5d19586d1c7120327e47e30a7a21931ed1e084fbfbdc41c82516e803893effae  kcalcore-18.12.3.tar.xz"
