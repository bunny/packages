# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=djvulibre
pkgver=3.5.27
pkgrel=1
pkgdesc="Format for distributing documents and images"
url="http://djvu.sourceforge.net/"
arch="all"
license="GPL-2.0+"
depends=""
depends_dev=""
makedepends="$depends_dev imagemagick libjpeg-turbo-dev tiff-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://downloads.sourceforge.net/djvu/djvulibre-$pkgver.tar.gz
	CVE-2019-15142.patch
	CVE-2019-15143.patch
	CVE-2019-15144.patch
	CVE-2019-15145.patch"

# secfixes:
#   3.5.27-r1:
#     - CVE-2019-15142
#     - CVE-2019-15143
#     - CVE-2019-15144
#     - CVE-2019-15145

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	# This doesn't actually do anything yet
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="62abcaa2fe7edab536477929ba38b882453dab1a06e119a3f838b38d5c61f5d8c252e4769e6534582b826e49bcfb490513179580fab9c3afa84aa92053ccebee  djvulibre-3.5.27.tar.gz
d9e4301fb98a35b8c2f1854eb4be53611f98b3fc9fdd357dd5502b5b189bdf61957a48b220f3ab7465bbf1df8606ce04513e10df74643a9e289c349f94721561  CVE-2019-15142.patch
3527e1c84f7c7d36f902cb3d7e9ddb6866acbdd4b47675ce3ffd164accf2e2931a4c6bbaae2ea775b4710d88ae34dd4dcd39a5846fce13bef2c82a99d608b8c1  CVE-2019-15143.patch
f8f1abf328a97d69514b2626e4c6449c0c7b7e2b5518d56bba6a61a944aaf4b7fffd1371c26396353728f6a1399c6d87492af5c17e6b623dae7751b81eac11f9  CVE-2019-15144.patch
790ef1e05874635c762600c990ecbd3e29e2eb01c59e25a0f8b2a15dbadbd3673d9dbb651d9dcb53fd3e5f4cb6bded47c3eefaaef8b4ccac39bd28f8bbec2068  CVE-2019-15145.patch"
