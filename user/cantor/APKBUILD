# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cantor
pkgver=19.08.1
pkgrel=0
pkgdesc="KDE worksheet interface for popular mathematical applications"
url="https://edu.kde.org/cantor/"
arch="all"
options="!check"  # Tests require X11.
license="GPL-2.0-only"
depends="r"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev karchive-dev kcompletion-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kio-dev knewstuff-dev kparts-dev kpty-dev ktexteditor-dev
	ktextwidgets-dev kxmlgui-dev libspectre-dev
	
	analitza-dev gfortran libqalculate-dev python3-dev r r-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/cantor-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b2f789905133880de99039cdae81698ed869860167f644c6898dd5e483eb0c4a479b6f5b3c5d2b6bfaddf30a4434224fdf517eaeb8aa77949e192c35b769f8ef  cantor-19.08.1.tar.xz"
