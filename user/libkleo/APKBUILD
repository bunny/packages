# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkleo
pkgver=19.08.1
pkgrel=0
pkgdesc="KDE encryption library"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND GPL-2.0-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev boost-dev gpgme-dev libgpg-error-dev ki18n-dev kwidgetsaddons-dev
	kitemmodels-dev"
makedepends="$depends_dev cmake extra-cmake-modules kcompletion-dev kconfig-dev
	kcodecs-dev kcoreaddons-dev kpimtextedit-dev kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/libkleo-$pkgver.tar.xz
	egregious-versions.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c956093662ac79544694a136203eda4a77600fdeabe8a88a55be47ba29f56b914dff28f355f07fc83e6e231e87686bccddab105089ff36755705c48b28c768c2  libkleo-19.08.1.tar.xz
5a10a4de59fc4e89295488c88f6f21f88f58f47679c666e0909bbd7bb6b3b867091292d807c2786da4864043d22c84e4f7cd7757fc47c9e046bedb8289c3c7b6  egregious-versions.patch"
