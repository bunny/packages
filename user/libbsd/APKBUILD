# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libbsd
pkgver=0.10.0
pkgrel=0
pkgdesc="Commonly used BSD functions not implemented by all libcs"
url="https://libbsd.freedesktop.org/"
arch="all"
license="BSD-2-Clause-NetBSD AND BSD-3-Clause AND BSD-4-Clause AND BSD-5-Clause AND Expat AND ISC AND Public-Domain AND Beerware"
depends="musl>=1.1.16-r22"
depends_dev="bsd-compat-headers linux-headers"
makedepends="$depends_dev autoconf automake libtool"
subpackages="$pkgname-dev $pkgname-doc"
source="https://libbsd.freedesktop.org/releases/$pkgname-$pkgver.tar.xz
	disable-fpurge-test.patch
	"

prepare() {
	default_prepare
	autoreconf -fi
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b75529785b16c93d31401187f8a58258fbebe565dac071c8311775c913af989f62cd29d5ce2651af3ea6221cffd31cf04826577d3e546ab9ca14340f297777b9  libbsd-0.10.0.tar.xz
424828e759420afbf2d4b97f5d402be9b3d51cb90241da1f047517bb75048255ea6cbbbc1e98bffa67449a7f48efe00d8f99b38bdaac9e8c461d1e989e9697b7  disable-fpurge-test.patch"
