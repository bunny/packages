# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=dracut
pkgver=048
pkgrel=3
pkgdesc="Event-driven initramfs infrastructure"
url="https://dracut.wiki.kernel.org/"
arch="all"
options="!check"  # Test suite is for kernel developers only, requires ext3 rootfs
license="GPL-2.0+"
depends="libarchive-tools musl-utils xz"
makedepends="fts-dev kmod-dev"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch
	$pkgname-crypt::noarch $pkgname-lvm::noarch"
source="https://www.kernel.org/pub/linux/utils/boot/$pkgname/$pkgname-$pkgver.tar.xz
	header-fix.patch
	mount-run-without-noexec.patch
	"

prepare() {
	default_prepare
	# Breaks separate /usr with OpenRC
	rm -r "$builddir"/modules.d/98usrmount
}

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	LDLIBS="-lfts" make
	make doc
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	for sysdmod in 00systemd 98dracut-systemd \
	    01systemd-initrd 02systemd-networkd; do
		rm -r "$pkgdir"/usr/lib/dracut/modules.d/$sysdmod
	done
}

bashcomp() {
	depends="dracut"
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion \
		"$subpkgdir"/usr/share
}

crypt() {
	depends="cryptsetup dracut lvm2"
	pkgdesc="$pkgname - LUKS / disk encryption support (crypt) module"
	mkdir -p "$subpkgdir"
}

lvm() {
	depends="dracut lvm2"
	pkgdesc="$pkgname - LVM2 module"
	mkdir -p "$subpkgdir"
}

sha512sums="97fcfd5d314ef40687c245d95d2f1d0f3f9ff0472e66b6e6324bf9bd6b98186104f9d71fd9af344126d6ea9fa47b744d52831a374225633225f6f17fb15c04e0  dracut-048.tar.xz
988f03a3fd2e7ee62409d3c57e8029403513dcec5efb37e64633d989728e4c7b619ce5b8775a04c5a0b654f7f093777d94fe6e4098a99a690c353a94f537e24c  header-fix.patch
d7aa2b35def975ec2a9620d3e8c94da5fad5be51e81ac913b9f3497b3ca62beefb9d4cf8e4ba3b292f89b936373486d0e3184f65eb1eaed972f38d17424a32b1  mount-run-without-noexec.patch"
