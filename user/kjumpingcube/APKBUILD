# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kjumpingcube
pkgver=19.08.1
pkgrel=0
pkgdesc="Dice-driven tactical game"
url="https://games.kde.org/game.php?game=kjumpingcube"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcoreaddons-dev kconfigwidgets-dev kcrash-dev kdbusaddons-dev kio-dev
	kdoctools-dev ki18n-dev kxmlgui-dev kwidgetsaddons-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kjumpingcube-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="d2aa9c0c4df85696cbbf066764b50e8bf6d449adc27833aacb4848cb0cb7cf0b6c2e2048ffc3f58d08fe8f727cb5635898a4e9b811760c878640d1bedda85bdf  kjumpingcube-19.08.1.tar.xz"
